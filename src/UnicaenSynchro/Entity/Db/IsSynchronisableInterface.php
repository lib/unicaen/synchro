<?php

namespace UnicaenSynchro\Entity\Db;

use DateTime;

interface IsSynchronisableInterface
{

    public function getInsertedOn(): ?DateTime;

    public function getUpdatedOn(): ?DateTime;

    public function getDeletedOn(): ?DateTime;

    public function isDeleted(?DateTime $date = null): bool;

    public function getSourceId(): ?string;
    public function getIdOrig(): ?string;

}