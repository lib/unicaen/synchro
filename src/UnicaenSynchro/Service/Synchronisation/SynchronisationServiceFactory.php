<?php

namespace UnicaenSynchro\Service\Synchronisation;

use Doctrine\ORM\EntityManager;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;
use UnicaenSynchro\Service\SqlHelper\SqlHelperService;

class SynchronisationServiceFactory {

    /**
     * @param ContainerInterface $container
     * @return SynchronisationService
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container) : SynchronisationService
    {
        // récupération des entity managers déclarés
        $sources = $container->get('Config')['doctrine']['entitymanager'];
        $entityManagers = [];
        foreach ($sources as $id => $data) {
            $entityManagers[$id] = $container->get('doctrine.entitymanager.'. $id);
        }

        /** @var SqlHelperService $sqlHelper */
        $sqlHelper = $container->get(SqlHelperService::class);
        $configs = $container->get('Config')['synchros'];

        $service = new SynchronisationService();
        $service->setSqlHelperService($sqlHelper);
        $service->setConfigs($configs);
        $service->setEntityManagers($entityManagers);
        return $service;
    }
}