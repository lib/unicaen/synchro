<?php

namespace UnicaenSynchro\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use UnicaenSynchro\Service\Synchronisation\SynchronisationServiceAwareTrait;

class SynchroniserCommand extends Command
{
    use SynchronisationServiceAwareTrait;


    private array $configs;

    public function setConfigs(array $configs): void
    {
        $this->configs = $configs;
    }

    protected static $defaultName = 'synchroniser';

    protected function configure(): void
    {
        $this->setDescription("Synchronisation de données sources");
        $this->addArgument('name', InputArgument::OPTIONAL, "Nom de la synchronisation");
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $name = $input->getArgument('name');


        if ($name !== null) {
            $io->title("Execution de la synchronisation [" . $name . "]");
            $this->getSynchronisationService()->synchronise($name);
        } else {
            $io->title("Synchronisation de données sources");
            $jobs = $this->configs;
            foreach ($jobs as $name => $job) {
                echo $this->getSynchronisationService()->synchronise($name);
            }
        }


        $io->success("Synchronisation·s effectuée·s");
        return self::SUCCESS;
    }
}