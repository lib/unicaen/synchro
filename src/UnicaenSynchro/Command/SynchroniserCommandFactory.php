<?php

namespace UnicaenSynchro\Command;

use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;
use Symfony\Component\Console\Command\Command;
use UnicaenSynchro\Service\Synchronisation\SynchronisationService;

class SynchroniserCommandFactory extends Command
{
    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container): SynchroniserCommand
    {
        /**
         * @var array $configs
         * @var SynchronisationService $synchroniserService
         */
        $configs = $container->get('Config')['synchros'];
        $synchronisationService = $container->get(SynchronisationService::class);

        $command = new SynchroniserCommand();
        $command->setSynchronisationService($synchronisationService);
        $command->setConfigs($configs);
        return $command;
    }
}
