<?php

namespace UnicaenSynchro;

use UnicaenSynchro\Service\SqlHelper\SqlHelperService;
use UnicaenSynchro\Service\SqlHelper\SqlHelperServiceFactory;

return [
    'service_manager' => [
        'factories' => [
            SqlHelperService::class => SqlHelperServiceFactory::class
        ],
    ],
];