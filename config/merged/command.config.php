<?php


use UnicaenSynchro\Command\SynchroniserCommand;
use UnicaenSynchro\Command\SynchroniserCommandFactory;

return [
    'bjyauthorize' => [
        'guards' => [
        ],
    ],

    'laminas-cli' => [
        'commands' => [
            'synchroniser' => SynchroniserCommand::class,
        ],
    ],

    'controllers' => [
        'factories' => [
        ],
    ],
    'service_manager' => [
        'factories' => [
            SynchroniserCommand::class => SynchroniserCommandFactory::class,
        ],
    ],

    'form_elements' => [
        'factories' => [
        ],
    ],
    'hydrators' => [
        'factories' => [
        ],
    ],
    'view_helpers' => [
        'invokables' => [
        ],
    ],
];

