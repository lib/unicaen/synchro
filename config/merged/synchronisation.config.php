<?php

namespace UnicaenSynchro;

use UnicaenSynchro\Service\Synchronisation\SynchronisationService;
use UnicaenSynchro\Service\Synchronisation\SynchronisationServiceFactory;

return [

    'service_manager' => [
        'factories' => [
            SynchronisationService::class => SynchronisationServiceFactory::class,
        ],
    ],
    'controllers' => [
        'factories' => [
        ],
    ],
    'form_elements' => [
        'factories' => [],
    ],
    'hydrators' => [
        'factories' => [],
    ]

];