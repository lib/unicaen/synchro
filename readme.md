Module Unicaen Synchronisation
=======================
------------------------

Description
-----------
Le module **unicaen/synchro** est en charge synchroniser des données de bases sources avec le modèle interne de l'application.
Le module est simple (voir simpliste).

Fonctionnement
==============

La bibliothèque s'utilise en console avec deux commandes : `synchroniser` et `synchroniser-all`

La commande `synchroniser-all` permet de lancer toutes les synchronisations.
```bash
$ php public/index.phtml synchroniser-all
```

La commande `synchroniser` permet de lancer la synchronisation donnée en paramètre (--name=). Attention la méthode est *case sensitive*.
```bash
$ php public/index.phtml synchroniser --name=CORRESPONDANCE_TYPE
```

Fichier de configuration
========================

Exemple de fichier de configuration :
```php
<?php

return [
    'synchros' => [
        'CORRESPONDANCE_TYPE' => [
            'order' => 1000,
            'source' => 'OCTOPUS',
            'orm_source' => 'orm_octopus',
            'orm_destination' => 'orm_default',
            'table_source' => 'V_EMC2_CORRESPONDANCE_TYPE',
            'table_destination' => 'carriere_correspondance_type',
            'correspondance' => [
                'ID' => 'id',
                'CODE' => 'code',
                'LIBELLE_COURT' => 'libelle_court',
                'LIBELLE_LONG' => 'libelle_long',
                'DESCRIPTION' => 'description',
                'DATE_DEBUT' => 'd_ouverture',
                'DATE_FIN' => 'd_fermeture',
            ],
            'id' => 'ID',
        ],
    ],
];

?>
```

La liste décrite dans `correspondance` sont les colonnes de la table source qui seront recopiées et vers quelles colonnes de la table destination elles iront.
La clef `id` sert à indiquer quelle colonne sera utilisée comme clef primaire.


Tables pour les données du module
==================================

Aucune pour le moment, car aucun log n'est enregistré !

Dépendances extérieures
======================

- **UnicaenPrivilege** : Déclaration des gardes pour les routes (pas sur que cela soit nécessaire).


Versions 
========

**6.0.3**
- Correction de l'interface/trait `IsSynchronisableInterface` et `IsSynchronisableTrait`
- Début de documentation

Futures améliorations
=====================

- Log de synchronisation
- Utilisation de la clef ordre pour l'execution des synchronisations `synchroniser-all`
- Changement de la clef id pour clef primaire ou quelque chose du genre
